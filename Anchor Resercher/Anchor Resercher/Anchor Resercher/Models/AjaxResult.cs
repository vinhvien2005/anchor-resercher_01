﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Anchor_Resercher.Models
{
    public class AjaxResult
    {
        public string Messages { get; set; }
        public bool Success { get; set; }
        public object Data { get; set; }

        public AjaxResult()
        {
            Messages = string.Empty;
            Success = true;
            Data = null;
        }
    }
}