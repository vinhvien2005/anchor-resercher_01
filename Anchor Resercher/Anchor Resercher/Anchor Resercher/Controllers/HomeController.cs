﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DBConnection.DAO;
using DBConnection.DTO;
using Anchor_Resercher.Models;
namespace Anchor_Resercher.Controllers
{
    public class HomeController : Controller
    {
        // define DAO
        TopPage_DAO topPageDAO = new TopPage_DAO();
        UrlListPage_DAO urlListPageDAO = new UrlListPage_DAO();
        DetailPage_DAO detailPageDAO = new DetailPage_DAO();
        DetailPageAnchor_DAO anchorDetailPageDAO = new DetailPageAnchor_DAO();
        //
        // GET: /Home/

        public ActionResult Index()
        {
            //MyConnection db = new MyConnection();
            //db.GetData("select  * from top_page");
            TopPage_DAO topPageDao = new TopPage_DAO();
            topPageDao.DeleteTopPage(new TopPage_DTO() {ID=3,KeyWord="test01",Status=1 });

            
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult SubmitInputKeyWord(string keywordData)
        {
            AjaxResult result = new AjaxResult();
            try
            {
                // check input data
                if (!string.IsNullOrEmpty(keywordData))
                {
                    string[] data = keywordData.Split('\n');
                    if (data.Length > 0)
                    {
                        for (int i = 0; i < data.Length; i++)
                        {
                            // insert keyword into database.
                            if (!string.IsNullOrEmpty(data[i]))
                            {
                                TopPage_DTO topPageDTO = new TopPage_DTO();
                                topPageDTO.Status = (int)TopPage_DTO.StatusTopPage.Todo;
                                topPageDTO.KeyWord = data[i];
                                // check exist topPageDTO
                                if (!topPageDAO.CheckExistTopPage(topPageDTO))
                                {
                                    // insert top_page into database
                                    try
                                    {
                                        topPageDAO.InsertTopPage(topPageDTO);
                                    }
                                    catch (Exception ex)
                                    {
                                        result.Messages += string.Format("insert keywrod {0} doesn't successed with error {1} !\n",topPageDTO.KeyWord,ex.Message);
                                    }
                                }
                                else
                                {
                                    result.Messages += string.Format("keyword {0} has existed ! \n", topPageDTO.KeyWord);
                                }
                            }
                        }
                        // get all top_page.
                        result.Data = topPageDAO.GetAllTopPage();
                    }
                }// key word is emmpty
            }
            catch (Exception ex)
            {
                ///
                result.Success = false;
                result.Messages += string.Format("{0} \n",ex.Message);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetServerStatus()
        {
            AjaxResult result = new AjaxResult();
            try
            {
                result.Data = topPageDAO.GetAllTopPage();
            }
            catch (Exception ex)
            {
                result.Messages = string.Format("get server status error : {0}",ex.Message);
                result.Success = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TopPage()
        {
            return View();
        }

        public ActionResult UrlListPage(int id)
        {
            // get all url by top_page ID
            List<UrlListPage_DTO> result = new List<UrlListPage_DTO>();
            try
            {
                result = urlListPageDAO.GetAllUrlByTopPageId(id);
            }
            catch (Exception ex)
            {
                // log error .
            }
            return View(result);
        }

        public ActionResult AnchorDetailPage(int id)
        {
            // get all detail page by url link
            UrlListPage_DTO urlListPageDTO = new UrlListPage_DTO();
            try
            {
                urlListPageDTO = urlListPageDAO.GetUrlById(id);
                if (urlListPageDTO.ID > 0)
                {
                    // get all anchor of detail page.
                    urlListPageDTO.ListAnchor = anchorDetailPageDAO.GetAllDetailPageAnchorByUrlId(urlListPageDTO.ID);
                }
            }
            catch (Exception ex)
            {
                // log error.
            }

            return View(urlListPageDTO);
        }

    }
    public class TestData
    {
        public int ID { get; set; }
        public string Data { get; set; }
    }
}