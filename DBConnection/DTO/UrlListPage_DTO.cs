﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBConnection.DTO
{
    public class UrlListPage_DTO
    {
        public int ID { get; set; }
        public int TopPageId { get; set; }
        public int Rank { get; set; }
        public string Title { get; set; }
        public string UrlValue { get; set; }
        public string PageTitle { get; set; }
        public string MetaDescription { get; set; }
        public List<DetailPageAnchor_DTO> ListAnchor { get; set; }

        public UrlListPage_DTO()
        {
            ListAnchor = new List<DetailPageAnchor_DTO>();
        }
    }
}
