﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBConnection.DTO
{
    public class DetailPage_DTO
    {
        public int ID { get; set; }
        public int UrlId { get; set; }
        public string Title { get; set; }
        public string MetaDescription { get; set; }
         public List<DetailPageAnchor_DTO> ListAnchor { get; set; }

         public DetailPage_DTO()
        {
            ListAnchor = new List<DetailPageAnchor_DTO>();
        }
    }
}
