﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBConnection.DTO
{
   public class TopPage_DTO
    {
        // status of topPage
        public enum StatusTopPage
        {
            Todo = 1,
            InProgressUrlListGenerate = 2,
            InProgressAnchorGenerate = 3,
            Done = 4
        }

        public int ID { get; set; }
        public string KeyWord { get; set; }
        public int Status { get; set; }
        public string InputDate { get; set; }
        public string StringStatus { get; set; }
    }
}
